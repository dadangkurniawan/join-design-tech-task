<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>JoinDesign - @yield('title')</title>

  <!-- Bootstrap core CSS -->
<link href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('js/datatables/datatables.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('toastr/toastr.min.css') }}" rel="stylesheet">
<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ URL::asset('css/simple-sidebar.css') }}" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/styles/metro/notify-metro.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css" rel="stylesheet">

</head>

<body>
    <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">JoinDesign </div>
        <div class="list-group list-group-flush">
            <a href="{{ url('product') }}" class="list-group-item list-group-item-action bg-light text-primary">Product</a>
            <a href="{{ url('promo-code') }}" class="list-group-item list-group-item-action bg-light text-primary">Promo code</a>
            <a href="{{ url('cart') }}" class="list-group-item list-group-item-action bg-light text-primary">Cart</a>
            <a href="{{ url('string-reducer') }}" class="list-group-item list-group-item-action bg-light text-primary">String Reducer</a>
            <a href="{{ url('employee') }}" class="list-group-item list-group-item-action bg-light text-primary">Somya Report</a>
            <a href="{{ url('tcpdf') }}" class="list-group-item list-group-item-action bg-light text-primary">TCPDF</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

        </nav>

        <div class="container-fluid">
            <div class="container">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
      <!-- Bootstrap core JavaScript -->
  <script src="{{ URL::asset('jquery/jquery.min.js') }}"></script>
  <script src="{{ URL::asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ URL::asset('js/jquery-blockui/jquery.blockui.js') }}"></script>
  <script src="{{ URL::asset('js/datatables/app.js') }}"></script>
  <script src="{{ URL::asset('js/datatables/datatables.all.min.js') }}"></script>
  <script src="{{ URL::asset('js/datatables/datatable.js') }}"></script>
  <script src="{{ URL::asset('toastr/toastr.min.js') }}"></script>
  <script src="https://kit.fontawesome.com/7e4fd09109.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js" crossorigin="anonymous"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

     function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function notify(param) {
        Command: toastr[param.type](param.message)

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }

    function dataJson(url) {
        var mydata = [];
        jQuery.ajax({
            url: url,
            async: false,
            dataType: 'json',
            success: function (json) {
                mydata = json.data;
            }
        });

        var json = jQuery.parseJSON(JSON.stringify(mydata));
        return json;
    }

  </script>
  @yield('script')
</body>

</html>
