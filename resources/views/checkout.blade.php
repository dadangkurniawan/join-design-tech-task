@extends('layouts.master') 
@section('title', 'Cart') 
@section('content')
<br>
<div class="row margin-top-10">
    <div class="col-md-12">
         <div class="pb-5">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

          <!-- Shopping cart table -->
          <div class="table-responsive">
            <table class="table" id="product-list">
              <thead>
                <tr>
                  <th scope="col" class="border-0 bg-light">
                    <div class="p-2 px-3 text-uppercase">Product</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase text-right">Price</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase text-center">Quantity</div>
                  </th>
                  <th scope="col" class="border-0 bg-light">
                    <div class="py-2 text-uppercase text-center">Remove</div>
                  </th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- End -->
        </div>
      </div>

      <div class="row py-5 p-4 bg-white rounded shadow-sm">
        <div class="col-lg-6">
          <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Coupon code</div>
          <div class="p-4">
            <p class="font-italic mb-4">If you have a coupon code, please enter it in the box below</p>
            <div class="input-group mb-4 border rounded-pill p-2">
              <input type="text" placeholder="Apply coupon" aria-describedby="btn-apply-promo-code" class="form-control border-0" id="apply-promo-code">
              <div class="input-group-append border-0">
                <button id="btn-apply-promo-code" type="button" onClick="applyPromoCode()" class="btn btn-primary px-4 rounded-pill"><i class="fa fa-gift mr-2"></i>Apply coupon</button>
                <button id="btn-remove-promo-code" type="button" onClick="removePromoCode()" class="btn btn-danger px-4 rounded-pill d-none"><i class="fa fa-trash mr-2"></i>Remove coupon</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold">Order summary </div>
          <div class="p-4">
            <p class="font-italic mb-4">Shipping and additional costs are calculated based on values you have entered.</p>
            <ul class="list-unstyled mb-4">
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Order Subtotal </strong><strong><span id="order-subtotal"></span></strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Shipping and handling</strong><strong>Rp. 0</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Tax</strong><strong>Rp. 0</strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Discount</strong><strong><span id="order-discount"></span></strong></li>
              <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">Total</strong>
                <h5 class="font-weight-bold"><span id="order-total"></span></h5>
              </li>
            </ul><a href="#" class="btn btn-primary text-white rounded-pill py-2 btn-block d-none">Procceed to checkout</a>
          </div>
        </div>
      </div>   
    </div>
</div>
@stop
@section('script')
<script>
     var addProduct = [];
     var discount = 0;
     var subTotal = 0;
     var percentage = 0;
     var promoCode = '';   

    jQuery('document').ready(function() {
       addProduct = JSON.parse(localStorage.getItem("cart"));
       showProduct();
       calculateOrder();

       jQuery('.btn-number').click(function(e){
            e.preventDefault();
            
            fieldName = jQuery(this).attr('data-field');
            type      = jQuery(this).attr('data-type');
            id      = jQuery(this).attr('data-id');

            var input = jQuery("input[name='"+fieldName+"']");
            var currentVal = parseInt(input.val());
            if (!isNaN(currentVal)) {
                if(type == 'minus') {
                    
                    if(currentVal > input.attr('min')) {
                        var qty = currentVal - 1;
                        input.val(qty).change();

                        productIndex = getProductIndex(id);
                        addProduct[productIndex].qty = qty;
                        calculateOrder();
                    } 
                    if(parseInt(input.val()) == input.attr('min')) {
                        jQuery(this).attr('disabled', true);
                    }

                } else if(type == 'plus') {

                    if(currentVal < input.attr('max')) {
                        var qty = currentVal + 1;
                        input.val(qty).change();

                        var productIndex = getProductIndex(id);
                        addProduct[productIndex].qty = qty;
                        calculateOrder();
                    }
                    if(parseInt(input.val()) == input.attr('max')) {
                        jQuery(this).attr('disabled', true);
                    }

                }
            } else {
                input.val(0);
            }
        });
        jQuery('.input-number').focusin(function(){
        jQuery(this).data('oldValue', jQuery(this).val());
        });
        jQuery('.input-number').change(function() {
            
            minValue =  parseInt(jQuery(this).attr('min'));
            maxValue =  parseInt(jQuery(this).attr('max'));
            valueCurrent = parseInt(jQuery(this).val());
            
            name = jQuery(this).attr('name');
            if(valueCurrent >= minValue) {
                jQuery(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
            } else {

                notify({
                    type: 'success',
                    message: 'Sorry, the minimum value was reached'
                });
                jQuery(this).val(jQuery(this).data('oldValue'));
            }
            if(valueCurrent <= maxValue) {
                jQuery(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
            } else {
                 notify({
                    type: 'success',
                    message: 'Sorry, the maximum value was reached'
                });
                jQuery(this).val(jQuery(this).data('oldValue'));
            }
            
            
        });
        jQuery(".input-number").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                    // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) || 
                    // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                        // let it happen, don't do anything
                        return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
    });

    function showProduct() {
        jQuery.each(addProduct, function(key, value) {
            var image = value.image ? value.image:'{{ URL::asset("img/default-product.png") }}';
            console.log(image);
            var html = '<tr id="item-product-' + value.id + '">'
             html += '<th scope="row" class="border-0">';
                html += '<div class="p-2">';
                    html += '<img src="' + image + '" alt="" width="70" class="img-fluid rounded shadow-sm">';
                    html += '<div class="ml-3 d-inline-block align-middle">';
                    html += '<h5 class="mb-0"> <a href="#" class="text-dark d-inline-block align-middle">' + value.name + '</a></h5>';
                    html += '</div>';
                html += '</div>';
                html += '</th>';
                html += '<td class="border-0 align-middle text-right"><strong>' + IDRFormat(value.price.toString(), 3) + '</strong></td>';
                // html += '<td class="border-0 align-middle text-center"><strong>' + value.qty + '</strong></td>';
                html += '<td class="border-0 align-middle text-center" width="180px">';
                    html += '<div class="input-group">';
                        html += '<span class="input-group-btn">'
                            html += '<button type="button" class="btn btn-primary btn-number" disabled="disabled" data-type="minus" data-id="' + value.id + '" data-field="quant[' + value.id + ']">';
                                html += '<span class="fa fa-minus"></span>';
                            html += '</button>';
                        html += '</span>';
                        html += '<input type="text" name="quant[' + value.id + ']" class="form-control input-number text-center" value="' + value.qty + '" min="1" max="10">';
                        html += '<span class="input-group-btn">';
                            html += '<button type="button" class="btn btn-primary btn-number" data-type="plus" data-id="' + value.id + '" data-field="quant[' + value.id + ']">';
                                html += '<span class="fa fa-plus"></span>';
                            html += '</button>';
                        html += '</span>';
                    html += '</div>';
                html += '</td>';
                html += '<td class="border-0 align-middle text-center"><a href="#" onClick="removeProduct(' + value.id + ')" class="text-primary" title="remove ' + value.name + '"><i class="fa fa-trash"></i></a></td>';
                html +='</tr>';
                jQuery(html).appendTo('#product-list');
        });
    }

    function calculateOrder() {
        subTotal = 0;
        total = 0;
        jQuery.each(addProduct, function(key, value) {
            subTotal += (value.price*value.qty);
        });

        discount = Math.ceil((subTotal * percentage) / 100);
        total = subTotal - discount;
        jQuery('#order-subtotal').text(IDRFormat(subTotal.toString(), 3));
        jQuery('#order-discount').text(IDRFormat(discount.toString(), 3));
        jQuery('#order-total').text(IDRFormat(total.toString(), 3));
    }


    function IDRFormat(price, prefix){
        var numberString = price.replace(/[^,\d]/g, '').toString(),
        split   		= numberString.split(','),
        rest     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, rest),
        thousand     		= split[0].substr(rest).match(/\d{3}/gi);
        
        if(thousand){
            separator = rest ? '.' : '';
            rupiah += separator + thousand.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
    function removeProduct(id) {
        const productIndex = getProductIndex(id);
         addProduct.splice(productIndex, 1);

        jQuery('#item-product-' + id).remove();
        calculateOrder();
        notify({
            type: 'success',
            message: 'product has been removed'
        });
    }

    function getProductIndex(productId) {
        return addProduct.findIndex(product => product.id == productId);
    }

    function applyPromoCode() {
            var param = {
                code: jQuery('#apply-promo-code').val()
            };

            jQuery.ajax({
                url: "{{ url('/api/promo-code/validation') }}",
                type: "POST",
                data: param,
                beforeSend: function() {
                    jQuery('.loading-event').fadeIn();
                },
                success: function(result) {
                    var data = result.data[0];
                    calculateDiscount(data.percentage, data.code, data.name);
                    jQuery("#btn-apply-promo-code").addClass('d-none'); 
                    jQuery('#btn-remove-promo-code').removeClass('d-none'); 
                    jQuery("#apply-promo-code").prop('disabled', 'disabled');
                     jQuery('#order-discount').addClass('text-primary');       
                     jQuery('#order-discount').prop('title', 'discount from ' + data.name);    
                    
                    notify({
                        type: 'success',
                        message: result.message
                    });
                },
                error: function(result){
                     notify({
                            type: 'warning',
                            message: result.responseJSON.message
                        });
                },
                complete: function() {
                    jQuery('.loading-event').fadeOut();
                }
            });
    }

    function calculateDiscount(percent, code, name) {
        percentage = percent;
        calculateOrder();
    }
    
    function removePromoCode() {
        discount = 0;
        percentage = 0;
        jQuery('#order-discount').removeClass('text-primary');    
        jQuery('#order-discount').prop('title', '');
        jQuery("#apply-promo-code").val('');
        jQuery("#apply-promo-code").prop('disabled', '');
        jQuery("#btn-apply-promo-code").removeClass('d-none'); 
        jQuery('#btn-remove-promo-code').addClass('d-none'); 
        calculateOrder();
    }
    
</script>

@stop