@extends('layouts.master') 
@section('title', 'Page Title') 
@section('content')
<br>
<div class="row margin-top-10">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              TCPDF
            </div>
            <div class="card-body">
             <p style="background: #f8f9faed;" class="p-5">
                    Image 1:
                    <code>
                        {{ public_path('/img/kamenrider.svg') }}
                    </code><br>
                    Image 2:
                    <code>
                        {{ public_path('/img/ponyo.png') }}
                    </code>
                </p>
                 <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="{{ url('tcpdf/generate') }}" allowfullscreen></iframe>
                </div>
          </div>
    </div>
</div>

@stop