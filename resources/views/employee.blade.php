@extends('layouts.master') 
@section('title', 'Page Title') 
@section('content')
<br>
<div class="row margin-top-10">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              Somya Report
            </div>
            <div class="card-body">
                <p style="background: #f8f9faed;" class="p-5">
                QUERY: <br>
                    <code>
                        SELECT <br>
                            &ensp;name, <br>
                            &ensp;salary,<br>
                            &ensp;REPLACE(ROUND(salary), "0", "") as salary_with_no_zero,<br>
                            &ensp;ROUND(salary -  REPLACE(ROUND(salary), "0", ""), 0) as salary_difference<br>
                        FROM employees
                    </code>
                </p>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">NAME</th>
                        <th scope="col">SALARY</th>
                        <th scope="col">SALARY WITH NO ZERO</th>
                        <th scope="col">SALARY DIFFERENCE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <th scope="row">{{ $employee->id }}</th>
                                <td>{{ $employee->name }}</td>
                                <td>{{ $employee->salary }}</td>
                                <td>{{ $employee->salary_with_no_zero }}</td>
                                <td>{{ $employee->salary_difference }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
            </div>
          </div>
    </div>
</div>

@stop