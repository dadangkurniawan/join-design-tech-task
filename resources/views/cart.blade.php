@extends('layouts.master') 
@section('title', 'Cart') 
@section('content')
<br>
<div class="row margin-top-10">
    <div class="col-md-8">
        <div class="container">
            <div class="row">
                @foreach($product as $list)
                    <div class="col-xs-12 col-sm-6 col-md-4">
                    <article class="card-wrapper">
                        <div class="image-holder">
                            <a href="#" onClick="addToCart({{ $list->id }}, '{{ strtoupper($list->name) }}', {{ $list->price }}, '{{ $list->image }}')" class="image-holder__link"></a>
                            <div class="image-liquid image-holder--original" style="background-image: url('{{ $list->image }}')">
                            </div>
                        </div>

                        <div class="product-description">
                            <!-- title -->
                            <h1 class="product-description__title">
                                <a href="#">						
                                    {{ strtoupper($list->name) }}
                                    </a>
                            </h1>

                            <!-- category and price -->
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 product-description__category secondary-text">
                                      Rp. {{ number_format($list->price,0,',','.') }}
                                </div>
                            </div>
                        </div>

                    </article>
                </div>
                @endforeach

            </div>
        </div>
    </div>
    <div class="col-md-4">
      <div class="container">
            <div class="row">
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">Summary <i class="fa fa-shopping-cart"></i></h5>
                    </div>
                    <ul class="list-group list-group-flush summary-product-list">
                    </ul>
                     <div class="card-body">
                        <button onClick="checkout()" class="btn btn-sm btn-primary text-white">Checkout</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
<script>
     var addProduct = [];
    jQuery('document').ready(function() {
       
    });

    function addToCart(id, name, price, image) {

        var existProduct = checkExistProduct(id);
        var qty = 0;

        if(existProduct) {
            var key = getProductIndex(id);
            qty = addProduct[key].qty + 1;
            addProduct[key].qty = qty;
            jQuery('#summary_product_name_' + id).text(name + ' x ' + qty);
            var status = 'updated';
        } else {
            addProduct.push({
                id: id, 
                name: name,
                price: price,
                image: image,
                qty: 1
            });
            qty = 1;
            var status = 'added';
            jQuery('<li class="list-group-item d-flex justify-content-between align-items-center" id="summary_product_' + id +'"><span id="summary_product_name_' + id + '">' + name + ' x ' + qty +'</span> <span class="btn btn-sm btn-secondary" title=" remove ' + name +'" onClick="removeProduct('+ id +')"><i class="fa fa-trash"></i></span></li>').appendTo('.summary-product-list');
        }
        
        notify({
            type: 'success',
            message: name + ' has been ' + status
        });
    }
    
    function removeProduct(id) {
        const productIndex = getProductIndex(id);
         addProduct.splice(productIndex, 1);

        jQuery('#summary_product_' + id).remove();
         notify({
            type: 'success',
            message: 'product has been removed'
        });
    }

    function checkExistProduct(id) {
        return addProduct.find(product => product.id === id);
    }

    function getProductIndex(productId) {
        return addProduct.findIndex(product => product.id === productId);
    }

    function checkout() {
        localStorage.setItem("cart", JSON.stringify(addProduct));
        window.location = "{{ url('checkout') }}";
    }

    
</script>

@stop