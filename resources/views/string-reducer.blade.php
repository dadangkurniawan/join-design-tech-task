@extends('layouts.master') 
@section('title', 'Page Title') 
@section('content')
<br>
<div class="row margin-top-10">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              String Reducer
            </div>
            <div class="card-body">
                    <div class="form-group mb-2">
                        <input type="text" class="form-control" name="name" id="name" >
                    </div>
                    <button type="button" onClick="stringReducer()" class="btn btn-primary mb-2">Reduce string</button>
                    <div class="form-group mb-2">
                        Result: <h1><span id="result-string"></span></h1>
                    </div>
            </div>
          </div>
    </div>
</div>

@stop
@section('script')
<script>
    jQuery('document').ready(function() {
        jQuery('#name').on('change', function() {
            stringReducer();
        })
    });

    function stringReducer() {
        var string = jQuery('#name').val();
        if(!string) {
            notify({
                type: 'warning',
                message: 'Please enter string'
            });
        } else {
            var stringArray = stringToArray(string);
            var countIndex = countArrayByIndex(string);
            var filteredString = removeArrayDuplicateValue(stringArray);
            var selectedString = [];
            
            jQuery.each(filteredString, function(key, val) {
                if(countIndex[val] % 2 != 0) {
                    selectedString.push(val);
                }
            });

            var result = selectedString.length > 0 ? selectedString.join():'Empty string';
            jQuery('#result-string').text(result.replace(/,/g,''));
        }
    }

    function checkString(string) {
        return string == document.getElementById("ageToCheck").value;
        }

    function stringToArray(string) {
        var stringArray = string.split('');

        return stringArray;
    }

    function removeArrayDuplicateValue(stringArray) {
        var filteredString = [];
        for(var i = 0; i < stringArray.length; i++) {
            if(jQuery.inArray(stringArray[i], filteredString) == -1) {
                filteredString.push(stringArray[i]);
            }
        }

        return filteredString;
    }


    function countArrayByIndex(stringArray) {
        var countIndex = [];
        for(var i = 0; i < stringArray.length; i++) {
            if(!countIndex[stringArray[i]]) {
                countIndex[stringArray[i]] = 1;
            } else {
                countIndex[stringArray[i]] = countIndex[stringArray[i]] + 1;
            }
        }

        return countIndex;
    }
    
</script>

@stop