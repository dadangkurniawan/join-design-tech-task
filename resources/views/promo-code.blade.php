@extends('layouts.master') 
@section('title', 'Page Title') 
@section('content')
<br>
<div class="row margin-top-10">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              Promo Code
            </div>
            <div class="card-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <span></span>
                        <button class="btn btn-sm btn-flat btn-primary btn-refresh-data-table" title="refresh"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-sm btn-flat btn-primary btn-add"><i class="fa fa-plus" title="Add new data"></i></button>
                    </div>
                    <table id="data-table" class="table table-condensed" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th>name</th>
                                <th>code</th>
                                <th>percentage</th>
                                <th></th>
                            </tr>
                            <tr role="row" class="filter">
                                <th><input type="text" class="form-control input-xs form-filter" name="name" autocomplete="off"></th>
                                <th><input type="text" class="form-control input-xs form-filter" name="code" id="flt_role" autocomplete="off"></th>
                                <th><input type="text" class="form-control input-xs form-filter" name="percentage" id="flt_role" autocomplete="off"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
          </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="add-data-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Create new</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form id="data-form">
            <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name <sup class="required">*</sup></label>
                        <input type="text" class="form-control" name="name" id="name" required>
                        <input type="hidden" name="id" id="id">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Code <sup class="required">*</sup></label>
                        <input type="text" class="form-control" autocomplete="off" name="code" id="code" style="text-transform:uppercase" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Percentage <sup class="required">*</sup></label>
                        <input type="text" autocomplete="off" class="form-control" name="percentage" id="percentage" onkeypress="return isNumber(event)" maxlength="2" required>
                    </div>
                   
            </div>
            <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@stop
@section('script')
<script>
    jQuery('document').ready(function() {
        var grid = new Datatable();
        grid.init({
            src: jQuery("#data-table"),
            onSuccess: function(grid) {},
            onError: function(grid) {},
            onDataLoad: function(grid) {},
            loadingMessage: 'Loading...',
            dataTable: {
                "dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions text-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150]
                ],
                "pageLength": 10,
                "ajax": {
                    url: "{!! route('api.promo-code-list') !!}"
                },
                columns: [
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'code',
                        name: 'code'
                    },
                    {
                        data: 'percentage',
                        name: 'percentage'
                    },
                    {
                        "render": function(data, type, row) {
                            content = '<button class="btn btn-flat btn-sm btn-primary btn-action btn-edit" title="edit data ' + row.id + '" onClick="edit(' + row.id + ')"><i class="fa fa-pencil"></i></button>';
                            if(row.status === 0) {
                                content += '<button class="btn btn-flat btn-sm btn-primary btn-action btn-inactivated" style="margin-left:5px"  onClick="updateStatus(' + row.id + ', 1)"><i class="fa fa-check"></i></button>';
                            } else {
                                content += '<button class="btn btn-flat btn-sm btn-primary btn-action btn-activated" style="margin-left:5px"  onClick="updateStatus(' + row.id + ', 0)"><i class="fa fa-trash"></i></button>';
                            }

                            return content;
                        }
                    }
                ],
                columnDefs: [
                    {
                        targets: [2],
                        className: 'text-right',
                        orderable: false,
                        width: '10%'
                    },
                    {
                        targets: [3],
                        className: 'text-center',
                        orderable: false,
                        width: '14%'
                    },
                ],
                oLanguage: {
                    sProcessing: "<div id='datatable-loader'></div>",
                    sEmptyTable: "Data Not found",
                    sLoadingRecords: ""
                },
                "order": [],
            }
        });


        jQuery('.btn-add').on('click', function() {
            document.getElementById("data-form").reset();
            jQuery('#add-data-modal').modal('show');
        });

        jQuery('#code').keypress(function(e) {
             if(e.which === 32) 
                return false;
        });

        jQuery('#data-form').on('submit', function(e) {
            e.preventDefault();
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var param = new FormData(this);
            var isUpdate = jQuery("#id").val();
            var apiSave = isUpdate ? 'update':'create';
            jQuery.ajax({
                url: "{{ url('/api/promo-code') }}/" + apiSave,
                type: "POST",
                data: param,
                contentType: false,
                processData: false,
                cache: false,
                beforeSend: function() {
                    jQuery('.loading-event').fadeIn();
                },
                success: function(result) {
                    if (result.success) {
                        jQuery("#add-data-modal").modal("hide");
                        jQuery("#data-table").DataTable().ajax.reload();
                        notify({
                            type: 'success',
                            message: result.message
                        });
                    } else {
                        notify({
                            type: 'warning',
                            message: result.message
                        });
                    }
                },
                complete: function() {
                    jQuery('.loading-event').fadeOut();
                }
            });
        });
    });

    function edit(id) {
        document.getElementById("data-form").reset();

        jQuery("#id").val(id);
        var result = jQuery.parseJSON(JSON.stringify(dataJson("{{ url('/api/promo-code/details/') }}/" + id)));
        jQuery("#id").val(result.id);
        jQuery("#name").val(result.name);
        jQuery("#code").val(result.code);
        jQuery("#percentage").val(result.percentage);
        
        jQuery("#add-data-modal .modal-title").html("<i class='fa fa-edit'></i> Update data");
        jQuery("#add-data-modal").modal("show");
    }

    function updateStatus(id, status) {
        jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var param = {
                id: id,
                status: status
            };

            jQuery.ajax({
                url: "{{ url('/api/promo-code/update-status') }}",
                type: "POST",
                data: param,
                beforeSend: function() {
                    jQuery('.loading-event').fadeIn();
                },
                success: function(result) {
                    if (result.success) {
                        jQuery("#add-data-modal").modal("hide");
                        jQuery("#data-table").DataTable().ajax.reload();
                        notify({
                            type: 'success',
                            message: result.message
                        });
                    } else {
                        notify({
                            type: 'warning',
                            message: result.message
                        });
                    }
                },
                complete: function() {
                    jQuery('.loading-event').fadeOut();
                }
            });
    }


    
</script>

@stop