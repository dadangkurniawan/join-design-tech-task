<?php

namespace App;

abstract class HttpStatusCode
{
    //Sucess
    const Success = 200;
    const SuccessCreate = 201;
    const SuccessDelete = 204;

    //Error - Client Side
    const BadRequest = 400;
    const Unauthorized = 401; //User is not authorized (User not logged in)
    const Forbidden = 403; //User doesn't have access to do the request (Logged in user doesn't have permission to access)
    const NotFound = 404;

     //Error - Server Side
     const InternalServerError = 500; //Internal Server Error
     const BadGateway = 502; //Upstream Server Error
}
