<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class TCPDFControler extends Controller
{
    public function index() {
       return view('tcpdf');
    }

    public function generatePDF() {
        
        PDF::SetTitle('Hello World');
        PDF::AddPage();
        PDF::ImageSVG(public_path('/img/kamenrider.svg'), 100, 175, 100, 100, '', '', 'T', false, 300, '', false, false, 1, false, false, false);
        PDF::AddPage();
        PDF::Image(public_path('/img/ponyo.png'), 50, 100, 100, 100, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

        PDF::Output('gibly.pdf');
    }
}
