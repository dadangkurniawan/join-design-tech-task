<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;

class CartController extends Controller
{
    public function index() {

        $request = Request::create('api/cart/product', 'GET');
        $response = Route::dispatch($request)->getContent();

        $product = json_decode($response)->data;

        return view('cart')->with(compact('product'));
    }
}
