<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class EmployeeController extends BaseController
{
    public function index() {
        $employee = DB::select('
            SELECT 
                id,
                name, 
                salary,
                REPLACE(ROUND(salary), "0", "") as salary_with_no_zero,
                ROUND(salary -  REPLACE(ROUND(salary), "0", ""), 0) as salary_difference
            FROM employees
        ');

        return $this->sendResponse($employee, 'Employees data rettrieved successfuly');
    }
}
