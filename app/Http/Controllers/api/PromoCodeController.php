<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\PromoCode as ResourcesPromoCode;
use App\Models\PromoCode;
use Illuminate\Http\Request;
use Validator;

class PromoCodeController extends BaseController
{
    /* Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
       $products = PromoCode::all();
       return $this->sendResponse(ResourcesPromoCode::collection($products), 'Promo code retrieved successfully.');
   }

   public function dataGrid(Request $request) {
        $orderColumn = $request->order[0]["column"];
        $dirColumn = $request->order[0]["dir"];
        $sortColumn = "";
        $selectedColumn[] = "";
        
        $selectedColumn = ['id', 'image', "name", "price", "status"];
        if($orderColumn) {
            $order = explode("as", $selectedColumn[$orderColumn]);
            if(count($order)>1) {
                $orderBy = $order[0]; 
            } else {
                $orderBy = $selectedColumn[$orderColumn];
            }
        
        }
        
        $promoCode= new PromoCode();
        $promoCode= $promoCode->select('id', 'name', 'code', 'percentage', 'status');
        $promoCode= $promoCode->where('status', 1);
        
        if ($request->name)
        $promoCode->where('name', 'like', '%'. $request->name .'%');
       
        if ($request->code)
        $promoCode->where('code', 'like', '%'. $request->code .'%');
        
        if ($request->percentage)
        $promoCode->where('percentage', 'like', '%'. $request->percentage .'%');
        
        $promoCode= $promoCode->get()->toArray();
        
        $iTotalRecords = count($promoCode);
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);
        $records = array();
        $records["data"] = array();
        
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $records["data"][] =  $promoCode[$i];
        }
        
        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }
        
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return response()->json($records);
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $input = $request->all();
       $validator = Validator::make($input, [
            'name' => 'required',
            'code' => 'required',
            'percentage' => 'required'
       ]);

       if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());
       }

       $input['created_at'] = date('Y-m-d H:i:s');
       $input['updated_at'] = date('Y-m-d H:i:s');
       $product = PromoCode::create($input);

       return $this->sendResponse(new ResourcesPromoCode($product), 'Promo Codecreated successfully.');
   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function show($id)
   {
       $promoCode = PromoCode::find($id);

       if (is_null($promoCode)) {
           return $this->sendError('Promo Code not found.');
       }

       return $this->sendResponse(new ResourcesPromoCode($promoCode), 'Promo Code retrieved successfully.');
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, PromoCode $promoCode)
   {
       $input = $request->all();

       $validator = Validator::make($input, [
           'id' => 'required',
           'name' => 'required',
           'code' => 'required',
           'percentage' => 'required'
       ]);

       if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());
       }
       $promoCode = PromoCode::find($input['id']);
       $promoCode->name = $input['name'];
       $promoCode->code = $input['code'];
       $promoCode->percentage = $input['percentage'];
       $promoCode->updated_at = date('Y-m-d H:i:s');
       $promoCode->save();

       return $this->sendResponse(new ResourcesPromoCode($promoCode), 'Promo Codeupdated successfully.');
   }

   public function updateStatus(Request $request) {
       $input = $request->all();

       $validator = Validator::make($input, [
           'id' => 'required',
           'status' => 'required'
       ]);

       if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());
       }

       try {
           $promoCode = PromoCode::find($input['id']);
           $promoCode->status = $input['status'];
           $promoCode->updated_at = date('Y-m-d H:i:s');
           $promoCode->save();

           return $this->sendResponse(new ResourcesPromoCode($promoCode), 'Promo Codestatus updated successfully.');
       } catch (\Throwable $th) {
           return $this->sendError('Process error', $th->getMessage());
       }
   }

   public function promoCodeValidation(Request $request)
   {
       $input = $request->all();
       $validator = Validator::make($input, [
            'code' => 'required'
       ]);

       if($validator->fails()){
           return $this->sendError('Validation Error.', $validator->errors());
       }

       $promoCode = PromoCode::where([
           ['status', 1],
           ['code', $input['code']]
       ])->get()->toArray();

       if(empty($promoCode)) {

           return $this->sendError('Promo code not found', $input['code']);
       }

       return $this->sendResponse(new ResourcesPromoCode($promoCode), 'Yeay! you got discount.');
   }
}
