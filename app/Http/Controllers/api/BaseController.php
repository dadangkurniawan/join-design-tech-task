<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\HttpStatusCode;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message, $code = HttpStatusCode::Success)
    {
    	$response = [
            'success' => true,
            'code'    => $code,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response,  $code);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = HttpStatusCode::InternalServerError)
    {
    	$response = [
            'success' => false,
            'code' => $code,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
}
