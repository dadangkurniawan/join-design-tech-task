<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product as ResourcesProduct;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;

class ProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return $this->sendResponse(ResourcesProduct::collection($products), 'Products retrieved successfully.');
    }

    public function dataGrid(Request $request) {
        $orderColumn = $request->order[0]["column"];
        $dirColumn = $request->order[0]["dir"];
        $sortColumn = "";
        $selectedColumn[] = "";

        $selectedColumn = ['id', 'image', "name", "price", "status"];
        if($orderColumn) {
            $order = explode("as", $selectedColumn[$orderColumn]);
            if(count($order)>1) {
                $orderBy = $order[0]; 
            } else {
                $orderBy = $selectedColumn[$orderColumn];
            }

        }

        $products = new Product;
        $products = $products->select('id', 'name', 'image', 'price', 'status');
        $products = $products->where('status', 1);

        if ($request->name)
        $products->where('name', 'like', '%'. $request->name .'%');
       
        if ($request->price)
        $products->where('price', 'like', '%'. $request->price .'%');

        $products = $products->get()->toArray();

        $iTotalRecords = count($products);
        $iDisplayLength = intval($request->length);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($request->start);
        $sEcho = intval($request->draw);
        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        for ($i = $iDisplayStart; $i < $end; $i++) {
            $records["data"][] =  $products[$i];
        }

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return response()->json($records);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'price' => 'required',
            'image' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if(isset($input['image_base64']) || isset($input['image'])) {
            $image = isset($input['image_base64']) ? $input['image_base64']: $this->uploadImageToBase64($_FILES["image"]["tmp_name"]);
            if(isset($input['image_base64'])) unset($input['image_base64']);
            
            $input['image'] = $image;
        }
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        $product = Product::create($input);

        return $this->sendResponse(new ResourcesProduct($product), 'Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if (is_null($product)) {
            return $this->sendError('Product not found.');
        }

        return $this->sendResponse(new ResourcesProduct($product), 'Product retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'image' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $product = Product::find($input['id']);
        $product->name = $input['name'];
        $product->price = $input['price'];

        if(isset($input['image_base64']) || isset($input['image'])) {
            $image = isset($input['image_base64']) ? $input['image_base64']: $this->uploadImageToBase64($_FILES["image"]["tmp_name"]);
            if(isset($input['image_base64'])) unset($input['image_base64']);
            
            $product->image = $image;
        }

        $product->updated_at = date('Y-m-d H:i:s');
        $product->save();

        return $this->sendResponse(new ResourcesProduct($product), 'Product updated successfully.');
    }

    public function updateStatus(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'id' => 'required',
            'status' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        try {
            $product = Product::find($input['id']);
            $product->status = $input['status'];
            $product->updated_at = date('Y-m-d H:i:s');
            $product->save();

            return $this->sendResponse(new ResourcesProduct($product), 'Product status updated successfully.');
        } catch (\Throwable $th) {
            return $this->sendError('Process error', $th->getMessage());
        }
    }

    public function uploadImageToBase64(String $path) {
        $path = $path;
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);

        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }
}
