<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\API\BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class CartController extends BaseController
{
    public function getProduct() {
        $product = Product::where('status', 1)->get();
        return $this->sendResponse($product, 'Product Retrieve successfully.');
    }
}
