<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = Request::create('api/employee', 'GET');
        $response = Route::dispatch($request)->getContent();

        $employees = json_decode($response)->data;
        return view('employee')->with(compact('employees'));
    }
}
