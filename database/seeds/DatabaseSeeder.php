<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Product;
use App\Models\PromoCode;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmployeeTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(PromoCodeTableSeeder::class);
        $this->command->info('Data table seeded!');
    }
}

class EmployeeTableSeeder extends Seeder {

    public function run()
    {
        DB::table('employees')->delete();
        $employees = array(
            array(
                "id" => 1,
                "name" => "Gavin",
                "salary" => 1420
            ),
            array(
                "id" => 2,
                "name" => "Norie",
                "salary" => 206
            ),
            array(
                "id" => 3,
                "name" => "Somya",
                "salary" => 2210
            ),
            array(
                "id" => 4,
                "name" => "Waiman",
                "salary" => 3000
            ),
        );

        foreach($employees as $employee) {
            Employee::create($employee);
        }
    }

}

class ProductTableSeeder extends Seeder {

    public function run()
    {
        DB::table('products')->delete();
        $products = array(
            array(
                "id" => 1,
                "name" => "Air Jordan  Noble Red",
                "price" => 1850000,
                "image" => "data:image/jpg;base64," . base64_encode( file_get_contents(public_path('/img/product/red.jpeg')))
            ),
            array(
                "id" => 2,
                "name" => "NIKE AIR JORDAN ROYAL BLUE BLACK",
                "price" => 2075000,
                "image" => "data:image/jpg;base64," . base64_encode( file_get_contents(public_path('/img/product/blue-black.jpeg')))
            ),
            array(
                "id" => 3,
                "name" => "Nike Air Jordan Pine Green ",
                "price" => 1900000,
                "image" => "data:image/jpg;base64," . base64_encode( file_get_contents(public_path('/img/product/pine-green.jpg')))
            )
        );

        foreach($products as $product) {
            Product::create($product);
        }
    }

}

class PromoCodeTableSeeder extends Seeder {

    public function run()
    {
        DB::table('promo_codes')->delete();
        $promoCodes = array(
            array(
                "id" => 1,
                "name" => "KARTINI",
                "code" => "KARTINI30",
                "percentage" => 30
            ),
            array(
                "id" => 2,
                "name" => "HAPPY EART DAY",
                "code" => "EARTDAY45",
                "percentage" => 45
            ),
        );

        foreach($promoCodes as $promoCode) {
            PromoCode::create($promoCode);
        }
    }

}
