<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('product');
});

Route::group(['prefix' => 'product'], function () {
    Route::get('', 'ProductController@index');
});

Route::group(['prefix' => 'promo-code'], function () {
    Route::get('', 'PromoCodeController@index');
});

Route::group(['prefix' => 'cart'], function () {
    Route::get('', 'CartController@index');
});

Route::group(['prefix' => 'checkout'], function () {
    Route::get('', 'CheckoutController@index');
});

Route::group(['prefix' => 'string-reducer'], function () {
    Route::get('', 'StringReducerController@index');
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('', 'EmployeeController@index');
});

Route::group(['prefix' => 'tcpdf'], function () {
    Route::get('', 'TCPDFControler@index');
    Route::get('generate', 'TCPDFControler@generatePDF');
});