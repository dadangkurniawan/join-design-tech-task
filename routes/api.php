<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'products'], function () {
    Route::get('', 'API\ProductController@index');
    Route::post('create', 'API\ProductController@store');
    Route::post('update-status', 'API\ProductController@updateStatus');
    Route::post('update', 'API\ProductController@update');
    Route::get('details/{id}', 'API\ProductController@show');
    Route::match(['get', 'post'], 'grid-product', [
        'as' => 'api.product-list',
        'uses' => 'API\ProductController@dataGrid'
    ]);
});

Route::group(['prefix' => 'promo-code'], function () {
    Route::get('', 'API\PromoCodeController@index');
    Route::post('create', 'API\PromoCodeController@store');
    Route::post('update-status', 'API\PromoCodeController@updateStatus');
    Route::post('update', 'API\PromoCodeController@update');
    Route::get('details/{id}', 'API\PromoCodeController@show');
    Route::post('validation', 'API\PromoCodeController@promoCodeValidation');
    Route::match(['get', 'post'], 'grid-promo-code', [
        'as' => 'api.promo-code-list',
        'uses' => 'API\PromoCodeController@dataGrid'
    ]);
});

Route::group(['prefix' => 'cart'], function () {
    Route::group(['prefix' => 'product'], function() {
        Route::get('', 'API\CartController@getProduct');
    });
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('', 'API\EmployeeController@index');
});