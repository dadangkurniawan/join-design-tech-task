# JOIN DESIGN #

### What is this repository for? ###

* this repository is used as a tech task JOIN DESIGN
* you can import `joinDesign.postman_collection.json` file to postman or other api tester

### Installation ###
* `git clone git@bitbucket.org:dadangkurniawan/join-design-tech-task.git`
* `cd join-design-tech-task`
* Run `composer install`
* Rename or copy `.env.example` file to `.env`
* Run `php artisan key:generate`
* Set your database credentials in your `.env` file
* Run `php artisan migrate`
* Run `php artisan db:seed`
* Run `php artisan serve`

### Credits
This project uses some open-source third-party libraries/packages, many thanks to the web community.
- Laravel - Open source framework
- Boostrap 4
- font awesome
- jQuery
- Datatable
- Toastr
- tcpdf

## Preview
`cart`
![ScreenShot](https://i.imgur.com/OwipL7U.png)

***

`tcpdf`
![ScreenShot](https://i.imgur.com/EORVK8B.png)
